#pragma once

#include "Common.h"
#include "Util.h"

class VariationalBayes {
 public:
    VariationalBayes(Corpus* corpus,
		     int num_topics,
		     int num_documents,
		     int batch_size,
		     double alpha,
		     double eta,
		     double tau0,
		     double kappa);
    ~VariationalBayes();

    void do_e_step(std::vector<int>& doc_index, int start_pos);
    void update_lambda(std::vector<int>& doc_index, int start_pos);
    double calculate_heldout_perplexity(std::vector<int>& doc_index,
					int start_pos,
					int end_pos);
    void dump();

 private:
    Corpus* corpus_;
    int num_topics_; // Number of topics
    int num_documents_; // Number of documents
    int num_words_; // Number of words in the vocabulary
    int batch_size_; // Number of documents in a minibatch
    int max_iter_; // Number of iteration in E step for every document
    double tau0_; // A (positive) learning parameter that downweights early iterations
    double kappa_; // exponential decay rate between (0.5, 1.0]
    double updatect_; // succeeded updates
    double meanchangethreshold_;
    double rhot_;
    
    double *alpha_; // Hyperparameter for prior on weight vectors theta
    double *eta_; // Hyperparameter for prior on topics beta, topic * vocalburary

    double *lambda_; // topic * vocabulary
    double *expElogbeta_; // topic * vocabulary
    double *sstats_; // topic * vocabulary

    double *theta_; // document * topic
    double *gamma_; // batch_size * topic
    double *expElogtheta_; // batch_size * topic
};
