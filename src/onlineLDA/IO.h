#pragma once

#include "Common.h"
#include "Util.h"

#include <string>

Corpus *read_corpus(std::string filename);

Corpus *transform_corpus(std::string filename);

void dump_model(double *theta, double *lambda, int D, int T, int W);
