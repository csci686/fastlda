#include <iostream>
#include <fstream>

#include "io.h"
#include "utils.h"

using namespace std;

int main()
{
	Corpus *corpus = read_corpus("../data/docword.nytimes.txt");

	cout << "read finished" << endl;

	ofstream fout;
	fout.open("test_corpus.txt");
	fout << corpus->docs.size() << endl; 
	fout << corpus->num_word << endl;
	
	fout << 69679427 << endl;

	for (int d = 0; d < corpus->docs.size(); ++d) {
		Doc *doc = corpus->docs[d];
		int count = 0;
		for (int i = 0; i < doc->words.size(); ++i) {
			if (i > 0 && doc->words[i] != doc->words[i - 1]) {
				fout << d + 1 << " " << doc->words[i - 1] + 1 << " " << count << endl;
				count = 0;
			}
			++count;
		}
		if (doc->words.size() > 0) {
			fout << d + 1 << " " << doc->words.back() + 1 << " " << count << endl;
		} else {
			cout << d << endl;
		}
	}

	fout.close();
	return 0;
}