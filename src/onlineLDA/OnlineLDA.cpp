#include "OnlineLDAMain.h"

#include "IO.h"

#include <cstdio>
#include <ctime>

int main(int argc, char *argv[])
{
	if (argc !=4 && argc != 5 && argc != 6) {
		printf("Usage: ./onlineLDA docword.txt iterations NumOfTopics\n");
		return 0;
	}

	std::string filename = std::string(argv[1]);
	int num_iterations = atoi(argv[2]);
	int num_topics = atoi(argv[3]);
	int batch_size = argc >= 5 ? atoi(argv[4]) : 8192;
	int tau = argc >= 6 ? atoi(argv[5]) : 32;

	printf("begin to read file\n");
	std::time_t begin = std::time(NULL);
	Corpus *corpus = read_corpus(filename);
        std::time_t end = std::time(NULL);
	int elapsed_secs = end - begin;
	printf("read finished, time elapsed: %ds\n", elapsed_secs);

	OnlineLDAMain *lda = new OnlineLDAMain(corpus,
					       num_iterations,
					       num_topics,
					       batch_size,
					       tau);
	lda->train_model();
        end = std::time(NULL);
	elapsed_secs = end - begin;
	printf("All finished, time elapsed: %ds\n", elapsed_secs);

	return 0;
}
