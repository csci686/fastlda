#pragma once

#include "VariationalBayes.h"

class OnlineLDAMain {
 public:
    OnlineLDAMain(Corpus* corpus,
		  int num_iterations,
		  int num_topics,
		  int batch_size,
		  int tau);

    void train_model();

 private:
    int num_documents_; // Number of documents
    int num_iterations_; // Number of sweeps
    int batch_size_; // Number of documents to analyze each mini-batch
    VariationalBayes* vb_; // The Underlying Varitional Bayesian Inference Process
};
