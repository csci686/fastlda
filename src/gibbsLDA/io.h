#ifndef _IO_H_
#define _IO_H_

#include "utils.h"

#include <string>

Corpus *read_corpus(std::string filename);

void dump_model(
	std::vector<std::vector<double> > &theta, 
	std::vector<std::vector<double> > &phi);

#endif