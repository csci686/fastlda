#pragma once

#include <vector>

struct Doc {
    // The first, word_index, says what vocabulary tokens are present in
    // each document. docs[i]->word_index[j] gives the jth unique token present in
    // document i. (Don't count on these tokens being in any particular
    // order.)

    // The second, word_count, says how many times each vocabulary token is
    // present. docs[i]->word_count[j] is the number of times that the token given
    // by docs[i]->word_count[j] appears in document i.

    std::vector<int> word_index;
    std::vector<double> word_count;
};

struct Corpus {
  	// Number of words in the dictionary
  	int num_word;

  	// Documents
  	std::vector<Doc *> docs;
};
