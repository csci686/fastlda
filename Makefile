fastLDA: OnlineLDA.o OnlineLDAMain.o VariationalBayes.o IO.o Common.o
	g++ -std=c++11 -o fastLDA OnlineLDA.o OnlineLDAMain.o VariationalBayes.o IO.o Common.o -fopenmp -O3

OnlineLDA.o: src/onlineLDA/OnlineLDA.cpp src/onlineLDA/OnlineLDAMain.h src/onlineLDA/VariationalBayes.h src/onlineLDA/Util.h src/onlineLDA/Common.h
	g++ -std=c++11 -c src/onlineLDA/OnlineLDA.cpp -O3

OnlineLDAMain.o: src/onlineLDA/OnlineLDAMain.cpp src/onlineLDA/OnlineLDAMain.h src/onlineLDA/VariationalBayes.h src/onlineLDA/Util.h src/onlineLDA/Common.h
	g++ -std=c++11 -c src/onlineLDA/OnlineLDAMain.cpp -O3

VariationalBayes.o: src/onlineLDA/VariationalBayes.cpp src/onlineLDA/VariationalBayes.h src/onlineLDA/Util.h src/onlineLDA/Common.h
	g++ -std=c++11 -c src/onlineLDA/VariationalBayes.cpp -fopenmp -O3

IO.o: src/onlineLDA/IO.cpp src/onlineLDA/IO.h src/onlineLDA/Util.h src/onlineLDA/Common.h
	g++ -std=c++11 -c src/onlineLDA/IO.cpp -O3

Common.o: src/onlineLDA/Common.cpp src/onlineLDA/Common.h
	g++ -std=c++11 -c src/onlineLDA/Common.cpp -O3

clean:
	rm -f fastLDA *.o *.txt