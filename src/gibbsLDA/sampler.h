#ifndef _SAMPLER_H_
#define _SAMPLER_H_

#include <vector>
#include <random>

class AliasTable {
  public:
	// Build the alias table
	AliasTable(std::vector<double> &p);
	
	// Update the alias table
	void update(std::vector<double> &p);
	
	// Sample a value according to the alias table
	int sample();

  private:
	// Size of the table
	int n;

	// Probabilities and aliases
	std::vector<double> prob;
	std::vector<int> alias;

	// Create an alias table according to the given probabilities
	void create_alias_table(std::vector<double> p);
};

// Sample a double value from uniform distribution [x, y]
double sample_uniform_double(double x, double y);

// Sample an int value from uniform distribution [x, y]
double sample_uniform_int(int x, int y);

#endif