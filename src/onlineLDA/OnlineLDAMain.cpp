#include "OnlineLDAMain.h"

#include <cstdio>
#include <ctime>
#include <iostream>

OnlineLDAMain::OnlineLDAMain(Corpus* corpus,
			     int num_iterations,
			     int num_topics,
			     int batch_size,
			     int tau) :
    num_iterations_(num_iterations),
    batch_size_(batch_size) {
    num_documents_ = corpus->docs.size();
    vb_ = new VariationalBayes(corpus,
			       num_topics,
			       corpus->docs.size(),
			       batch_size,
			       1.0 / num_topics,
			       1.0 / num_topics,
			       1.0 * tau,
			       0.5);
}

void OnlineLDAMain::train_model() {
    std::vector<int> randomPermutation(num_documents_);
    for(int i = 0; i < num_documents_; i++)
	randomPermutation[i] = i;
    
    //int num_minibatch = 1;
    int num_minibatch = (num_documents_ - 1) / batch_size_ + 1;
    int num_iter = (num_iterations_ - 1) / num_minibatch + 1;
    std::cout << "#batches " << num_minibatch << std::endl;

    for(int iter = 0; iter < num_iter; iter++) {
	std::time_t begin = std::time(NULL);

	// Shuffle documents randomly 
	std::random_shuffle(randomPermutation.begin(), randomPermutation.end());

	// Feed a bunch of minibatches to OnlineLDA Model
	for(int batch_index = 0; batch_index < num_minibatch; batch_index++) {
	    vb_->update_lambda(randomPermutation, batch_index * batch_size_);
	}

        std::time_t end = std::time(NULL);
	int elapsed_secs = end - begin;
	printf("Round %d finished, time elapsed: %ds\n", iter + 1, elapsed_secs);
        /*
	double perplexity = vb_->calculate_heldout_perplexity(
	    randomPermutation, 0, num_documents_);
	printf("Round %d finished, time elapsed: %ds, perplexity: %.2f\n",
	       iter + 1, elapsed_secs, perplexity);
	*/
	// Save model to disk
	vb_->dump();
    }    
}
