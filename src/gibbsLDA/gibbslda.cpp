#include "gibbslda.h"
#include "io.h"

#include <cstdio>
#include <ctime>
#include <future>
#include <algorithm> 
#include <omp.h>

// Train the model using T topics for num_round rounds given corpus
void GibbsLDA::train_model(Corpus *corpus, int num_round, int num_topic)
{
	printf("begin to train\n");

	// Initialize parameters of the model
	init_params(corpus, num_topic);

	// Iterate over the corpus for num_round rounds
	for (int r = 0; r < num_round; ++r) {
		std::time_t begin = std::time(NULL);

		#pragma omp parallel
    	{
        	#pragma omp single
        	{
         		for (int i = 0; i < num_thread; ++i) {
					#pragma omp task
					sample_topic(i);
				}
				#pragma omp taskwait   
        	}
   	 	}

		std::time_t end = std::time(NULL);

		// Estimate model parameters
		estimate_model_params();

		int elapsed_secs = end - begin;
		// double likelihood = calculate_log_likelihood(corpus, theta, phi);
		// printf("round %d finished, time elapsed: %ds likelihood:%.2f\n", 
		// 	r + 1, elapsed_secs, likelihood);
		printf("round %d finished, time elapsed: %ds\n", 
			r + 1, elapsed_secs);
	}

	dump_model(theta, phi);
}

// Initialize parameters of the model
void GibbsLDA::init_params(Corpus *corpus, int num_topic)
{
	this->corpus = corpus;
	D = corpus->docs.size();
	W = corpus->num_word;
	T = num_topic;
	
	alpha = 50.0 / T;
	beta = 0.1;
	num_thread = 4;

	nd = std::vector<std::vector<int> >(D);
	for (int d = 0; d < D; ++d) {
		nd[d] = std::vector<int>(T, 0);
	}

	nw = std::vector<std::vector<int> >(W);
	for (int w = 0; w < W; ++w) {
		nw[w] = std::vector<int>(T, 0);
	}

	topic_sum = std::vector<int>(T, 0);
	doc_sum = std::vector<int>(D);

	z = std::vector<std::vector<int> >(D);
	for (int d = 0; d < D; ++d) {
		Doc *doc = corpus->docs[d];
		z[d] = std::vector<int>(doc->words.size());
		for (int i = 0; i < doc->words.size(); ++i) {
			int k = sample_uniform_int(0, T - 1);
			int w = doc->words[i];
			z[d][i] = k;
			++nd[d][k];
			++nw[w][k];
			++topic_sum[k];
		}
		doc_sum[d] = doc->words.size();
	}

	theta = std::vector<std::vector<double> >(D);
	for (int d = 0; d < D; ++d) {
		theta[d] = std::vector<double>(T);
	}

	phi = std::vector<std::vector<double> >(W);
	for (int w = 0; w < W; ++w) {
		phi[w] = std::vector<double>(T);
	}
}

// Sample z[d][i] using Metropolis-Hastings sampling
void GibbsLDA::sample_topic(int start)
{
	printf("schdule thread %d on cpu %d\n", omp_get_thread_num(), sched_getcpu());
	for (int d = start; d < D; d += num_thread) {
		Doc *doc = corpus->docs[d];
		for (int i = 0; i < doc->words.size(); ++i) {
			int w = doc->words[i];
			
			int s = z[d][i];
			--nd[d][s];
			#pragma omp atomic
			--nw[w][s];
			#pragma omp atomic
			--topic_sum[s];
			
			std::vector<double> p(T);
			double sum = 0;
			for (int k = 0; k < T; ++k) {
				sum += (nw[w][k] + beta) * (nd[d][k] + alpha) / (topic_sum[k] + W * beta);
				p[k] = sum;
			}
			
			int t;
			double ran = sample_uniform_double(0, sum);
			for (int k = 0; k < T; ++k) {
				if (p[k] > ran) {
					t = k;
					break;
				}
 			}
 			
 			z[d][i] = t;	
			++nd[d][t];
			#pragma omp atomic
			++nw[w][t];
			#pragma omp atomic
			++topic_sum[t];
		}
	}
}

// Estimate model parameters
void GibbsLDA::estimate_model_params()
{
	for (int k = 0; k < T; ++k) {
		for (int d = 0; d < D; ++d) {
			theta[d][k] = (nd[d][k] + alpha) / (doc_sum[d] + T * alpha);
		}
		for (int w = 0; w < W; ++w) {
			phi[w][k] = (nw[w][k] + beta) / (topic_sum[k] + W * beta);
		}
	}
}
