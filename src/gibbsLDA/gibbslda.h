#ifndef _GIBBSLDA_H_
#define _GIBBSLDA_H_

#include "utils.h"
#include "sampler.h"

#include <vector>

class GibbsLDA {
  public:
	// Train the model using T topics for num_round rounds given corpus
	void train_model(Corpus *corpus, int num_round, int num_topic);

  private:
  	// theta[d][k] is the probability of topic k for document d
	std::vector<std::vector<double> > theta;

	// phi[w][k] is the probability of wth word in vocabulary assigned to topic k
	std::vector<std::vector<double> > phi;

	// z[d][i] is the topic assigned to ith word in dth document
	std::vector<std::vector<int> > z;

	// nd[d][k] is the number of tokens in document d that are assigned to topic k
	std::vector<std::vector<int> > nd;

	// nw[w][k] is the number of tokens with word w that are assigned to topic k
	std::vector<std::vector<int> > nw;
	
	// topic_sum[k] is the number of tokens across all docs assigned to topic k
	std::vector<int> topic_sum;

	// doc_sum[d] is the number of tokens in document d
	std::vector<int> doc_sum;

	// Parameter of the Drichlet distribution to draw theta
	double alpha;

	// Parameter of the Drichlet distribution to draw phi
	double beta;

	// Number of topics
	int T;

	// Number of documents
	int D;

	// Number of words in the dictionary
	int W;

	// Number of threads;
	int num_thread;

	// Corpus of the model
	Corpus *corpus;

	// Initialize parameters of the model
	void init_params(Corpus *corpus, int num_topic);

	// Do sampling
	void sample_topic(int start);

	// Estimate model parameters
	void estimate_model_params();
};

#endif