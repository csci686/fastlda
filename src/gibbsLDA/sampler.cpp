#include "sampler.h"

#include <vector>
#include <queue>

// Build the alias table
AliasTable::AliasTable(std::vector<double> &p) 
{
	create_alias_table(p);
}

// Update the alias table
void AliasTable::update(std::vector<double> &p) 
{
	create_alias_table(p);
}

// Create an alias table according to the given probabilities
void AliasTable::create_alias_table(std::vector<double> p) 
{
	n = p.size();
	alias = std::vector<int>(n);
	prob = std::vector<double>(n);

	const double average = 1.0 / n;

	std::queue<int> S, L;
	for (int i = 0; i < n; ++i) {
		if (p[i] >= average) {
			L.push(i);
		} else {
			S.push(i);
		}
	} 

	while (!S.empty() && !L.empty()) {
		int a = S.front(), g = L.front();
		S.pop();
		L.pop();
		prob[a] = p[a] * n;
		alias[a] = g;
		p[g] = p[g] + p[a] - average;
		if (p[g] >= average) {
			L.push(g);
		} else {
			S.push(g);
		}
	}

	while (!S.empty()) {
		int a = S.front();
		S.pop();
		prob[a] = 1.0;
	}

	while (!L.empty()) {
		int a = L.front();
		L.pop();
		prob[a] = 1.0;
	}
}

// Sample a value according to the alias table
int AliasTable::sample()
{
	int i = sample_uniform_int(0, n - 1);
	double ran = sample_uniform_double(0.0, 1.0);
	return ran < prob[i] ? i : alias[i];
}

// Sample a double value from uniform distribution [x, y]
double sample_uniform_double(double x, double y)
{
	return x + rand() / ((double)RAND_MAX + 1) * (y - x);
}

// Sample an int value from uniform distribution [x, y]
double sample_uniform_int(int x, int y)
{
	return x + rand() / ((double)RAND_MAX + 1) * (y - x + 1);
}
