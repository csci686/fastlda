#include "IO.h"

#include <cstdio>
#include <string>

Corpus *read_corpus(std::string filename)
{
    // FOR docword.enron.txt
    // Using iostream, 36 secs
    // Using C FILE, 11 secs
    // FOR docword.nytimes.txt
    // Using iostream, ??? secs
    // Using C FILE, 202 secs

    FILE *fp;
    const char *cfilename = filename.c_str();
    fp = fopen(cfilename, "r");

    if (fp != NULL) {
	int D, W, N, ret;
	ret = fscanf(fp, "%d%d%d", &D, &W, &N);

	Corpus *corpus = new Corpus();
	corpus->num_word = W;

	corpus->docs = std::vector<Doc *>(D);
	for (int i = 0; i < D; ++i) {
	    Doc *doc = new Doc();
	    corpus->docs[i] = doc;
	}	

	for (int k = 0; k < N; ++k) {
	    int d, w, n;
	    ret = fscanf(fp, "%d%d%d", &d, &w, &n);
	    Doc *doc = corpus->docs[d - 1];
	    doc->word_index.push_back(w - 1);
	    doc->word_count.push_back(n);
        }

	fclose(fp);
	return corpus;
    } else {
	return NULL;
    }
}

Corpus *transform_corpus(std::string filename) 
{
    FILE *fp, *fpout;
    const char *cfilename = filename.c_str();
    fp = fopen(cfilename, "r");
    fpout = fopen("enron.txt", "w");

    if (fp != NULL && fpout != NULL) {
	int D, W, N, ret;
	ret = fscanf(fp, "%d%d%d", &D, &W, &N);

	Corpus *corpus = new Corpus();
	corpus->num_word = W;

	std::vector<int> index;
	std::vector<int> count;
	int prev = -1, tot = 0;
        for (int k = 0; k < N; ++k) {
	    int d, w, n;
	    ret = fscanf(fp, "%d%d%d", &d, &w, &n);

	    if (prev != d && prev != -1) {
		fprintf(fpout, "%d", tot);
		for (int j = 0; j < tot; ++j) {
		    fprintf(fpout, " %d:%d", index[j], count[j]);
		}
	        fprintf(fpout, "\n");
		index.clear();
		count.clear();
		tot = 0;
	    }
			
	    prev = d;
	    tot++;
	    index.push_back(w);
	    count.push_back(n);
        }

        if (tot) {
	    fprintf(fpout, "%d", tot);
       	    for (int j = 0; j < tot; ++j) {
       	        fprintf(fpout, " %d:%d", index[j], count[j]);
	    }
	    fprintf(fpout, "\n");
	    index.clear();
	    count.clear();
	    tot = 0;
        }

	fclose(fp);
	fclose(fpout);
	return corpus;
    } else {
	return NULL;
    }
}

void dump_model(double *theta, double *lambda, int D, int T, int W) 
{
    // Normalize theta, lambda
    std::vector<double> theta_norm(D);
    std::vector<double> lambda_norm(T);
    for(int d = 0; d < D; ++d) {
	for(int k = 0; k < T; ++k) {
	    theta_norm[d] += Matrix(theta, d, k, T);
	}
    }
    for(int k = 0; k < T; ++k) {
	for(int j = 0; j < W; ++j) {
	    lambda_norm[k] += Matrix(lambda, k, j, W);
	}
    }

    FILE *fp;
    std::string filename;

    filename = "doctopic.txt";
    fp = fopen(filename.c_str(), "w");
    if (fp != NULL) {
	for (int d = 0; d < D; ++d) {
	    for (int k = 0; k < T; ++k) {
    		fprintf(fp, "%.6f", Matrix(theta, d, k, T) / theta_norm[d]);
		if (k < T - 1) {
		    fprintf(fp, ", ");
		} else {
		    fprintf(fp, "\n");
		}
	    }
	}
	fclose(fp);
    }

    filename = "topics.txt";
    fp = fopen(filename.c_str(), "w");
    if (fp != NULL) {
	for (int k = 0; k < T; ++k) {
             int firstW = 100;
	     auto cmp = [&lambda, &k, &W](int x, int y) {
		 return Matrix(lambda, k, x, W) > Matrix(lambda, k, y, W);
	     };
	     std::vector<int> word_index;

	     for (int w = 0; w < W; ++w) {
   		  word_index.push_back(w);
		  std::push_heap(word_index.begin(), word_index.end(), cmp);

		  if (w >= firstW) {
    		      std::pop_heap(word_index.begin(), word_index.end(), cmp);
         	      word_index.pop_back();
		  }
	     }

	     std::sort(word_index.begin(), word_index.end(), cmp);

	     for (int i = 0; i < firstW; ++i) {
    		  int w = word_index[i];
		  fprintf(fp, "%d:%.6f", w + 1, Matrix(lambda, k, w, W) / lambda_norm[k]);
		  if (i == firstW - 1) {
		      fprintf(fp, "\n");
		  } else {
		      fprintf(fp, ", ");
		  }
	     }
	}
	fclose(fp); 
    }
}

