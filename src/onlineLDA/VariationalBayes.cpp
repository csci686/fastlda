#include "VariationalBayes.h"

#include "IO.h"

#include <chrono>

#include "stdlib.h"
#include "omp.h"

boost::random::mt19937 rng;
boost::random::gamma_distribution<> draw_gamma(100.0, 1.0 / 100);

VariationalBayes::VariationalBayes(Corpus* corpus,
				   int num_topics,
				   int num_documents,
				   int batch_size,
				   double alpha,
				   double eta,
				   double tau0,
				   double kappa) :
    corpus_(corpus),
    num_topics_(num_topics),
    num_documents_(num_documents),
    num_words_(corpus->num_word),
    batch_size_(batch_size),
    max_iter_(20),
    tau0_(tau0),
    kappa_(kappa),
    updatect_(0.0),
    meanchangethreshold_(1e-4),
    rhot_(0.0) {

    alpha_ = (double*)malloc(sizeof(double) * num_topics);
    for(int i = 0; i < num_topics; ++i) {
	Vector(alpha_, i) = alpha;
    }
    eta_ = (double*)malloc(sizeof(double) * num_topics * corpus->num_word);
    for(int i = 0; i < num_topics * corpus->num_word; ++i) {
	Vector(eta_, i) = eta;
    }
    
    theta_ = (double*)malloc(sizeof(double) * num_documents * num_topics);
    for(int i = 0; i < num_documents * num_topics; ++i) {
	Vector(theta_, i) = 1.0 / num_topics;
    }

    gamma_ = (double*)malloc(sizeof(double) * batch_size * num_topics);
    expElogtheta_ = (double*)malloc(sizeof(double) * batch_size * num_topics);

    lambda_ = (double*)malloc(sizeof(double) * num_topics * corpus->num_word);
    expElogbeta_ = (double*)malloc(sizeof(double) * num_topics * corpus->num_word);
    sstats_ = (double*)malloc(sizeof(double) * num_topics * corpus->num_word);

    // Initialize the variational distribution q(beta|lambda)
    for (int i = 0; i < num_topics * corpus->num_word; ++i) {
        Vector(lambda_, i) = draw_gamma(rng);
    }
    exp_dirichlet_expectation_Matrix(expElogbeta_, lambda_, num_topics, corpus->num_word);
}

VariationalBayes::~VariationalBayes() {
    free(alpha_);
    free(eta_);
    free(lambda_);
    free(expElogbeta_);
    free(sstats_);
    free(theta_);
    free(gamma_);
    free(expElogtheta_);
}

void VariationalBayes::do_e_step(std::vector<int>& doc_index, int start_pos) {
    // Given a mini-batch of documents, estimates the parameters
    // gamma controlling the variational distribution over the topic
    // weights for each document in the mini-batch and
    // compute sufficient statistics stats_ needed to update lambda.

    // Initialize the variational distribution q(theta|gamma) for
    // the mini-batch
    for (int i = 0; i < batch_size_ * num_topics_; ++i) {
        Vector(gamma_, i) = draw_gamma(rng);
    }
    exp_dirichlet_expectation_Matrix(expElogtheta_, gamma_, batch_size_, num_topics_);

    for (int i = 0; i < num_topics_ * num_words_; ++i) {
        Vector(sstats_, i) = 0.0;
    }

    // Now, for each document d update that document's gamma and phi
    int num_docs = std::min(batch_size_, num_documents_ - start_pos);
#pragma omp parallel for
    for(int i = 0; i < num_docs; ++i) {
	int d = doc_index[start_pos + i];

	std::vector<int>& word_index = corpus_->docs[d]->word_index;
	std::vector<double>& word_count = corpus_->docs[d]->word_count;
	int doc_len = word_index.size();

	// a row in the matrix
	double *gamma_doc = (double*)malloc(sizeof(double)* num_topics_);
	double *last_gamma_doc = (double*)malloc(sizeof(double)* num_topics_);
	double *expElogtheta_doc = (double*)malloc(sizeof(double)* num_topics_);

	for(int k = 0; k < num_topics_; ++k) {
	    Vector(gamma_doc, k) = Matrix(gamma_, i, k, num_topics_);
	    Vector(expElogtheta_doc, k) = Matrix(expElogtheta_, i, k, num_topics_);
	}

	// The optimal phi_{dwk} is proportional to 
        // expElogthetad_k * expElogbetad_w. phinorm is the normalizer.
	double *phinorm = (double*)malloc(sizeof(double) * doc_len);
	for(int j = 0; j < doc_len; ++j) {
	    Vector(phinorm, j) = 1e-10;
	    for(int k = 0; k < num_topics_; ++k) {
		Vector(phinorm, j) += Vector(expElogtheta_doc, k) *
		    Matrix(expElogbeta_, k, word_index[j], num_words_);
	    }
	}

        // Iterate between gamma and phi until convergence
	for(int iter = 0; iter < max_iter_; ++iter) {
	    for(int k = 0; k < num_topics_; ++k) {
		Vector(last_gamma_doc, k) = Vector(gamma_doc, k);
	    }

	    for(int k = 0; k < num_topics_; ++k) {
		Vector(gamma_doc, k) = 0.0;
		for(int j = 0; j < doc_len; ++j) {
		    Vector(gamma_doc, k) += word_count[j] / Vector(phinorm, j) * 
			Matrix(expElogbeta_, k, word_index[j], num_words_); 
		}
		Vector(gamma_doc, k) = Vector(alpha_, k) + Vector(expElogtheta_doc, k) *
		    Vector(gamma_doc, k);
	    }

	    exp_dirichlet_expectation_Vector(expElogtheta_doc, gamma_doc, num_topics_);

	    for(int j = 0; j < doc_len; ++j) {
		Vector(phinorm, j) = 1e-10;
		for(int k = 0; k < num_topics_; ++k) {
		    Vector(phinorm, j) += Vector(expElogtheta_doc, k) *
			Matrix(expElogbeta_, k, word_index[j], num_words_);
		}
	    }

            // If gamma hasn't changed much, we're done.
	    double meanchange = 0.0;
	    for(int k = 0; k < num_topics_; ++k) {
		meanchange += std::fabs(Vector(gamma_doc, k) - Vector(last_gamma_doc, k));
	    }
	    meanchange /= num_topics_;
	    if (meanchange < meanchangethreshold_) break;	 
	}
	
	for(int k = 0; k < num_topics_; ++k) {
	    Matrix(gamma_, i, k, num_topics_) = Vector(gamma_doc, k);
	    Matrix(theta_, d, k, num_topics_) = Vector(gamma_doc, k);
	}

        // Contribution of document d to the expected sufficient
        // statistics for the M step.
#pragma omp critical
{
	for(int k = 0; k < num_topics_; ++k) {
	    for(int j = 0; j < doc_len; ++j) {
		Matrix(sstats_, k, word_index[j], num_words_) += Vector(expElogtheta_doc, k) *
		    word_count[j] / Vector(phinorm, j);
	    }
	}
}

	free(gamma_doc);
	free(last_gamma_doc);
	free(expElogtheta_doc);
	free(phinorm);
    }

    for(int i = 0; i < num_topics_ * num_words_; ++i) {
        Vector(sstats_, i) *= Vector(expElogbeta_, i);
    }

    std::cout << "E step succeded." << std::endl;
}

void VariationalBayes::update_lambda(std::vector<int>& doc_index, int start_pos) {
    // First does an E step on the mini-batch given in word_index and
    // word_count, then uses the result of that E step to update the
    // variational parameter matrix lambda.

    std::chrono::time_point<std::chrono::system_clock> start, end;
    start = std::chrono::system_clock::now();

    // rhot will be between 0 and 1, and says how much to weight
    // the information we got from this mini-batch.
    rhot_ = std::pow(tau0_ + updatect_, -kappa_);

    // Do an E step to update gamma, phi | lambda for this
    // mini-batch. This also returns the information about phi that
    // we need to update lambda.
    do_e_step(doc_index, start_pos);

    // Update lambda based on documents.
    double coef = 1.0 * num_documents_ / batch_size_;
    for(int i = 0; i < num_topics_ * num_words_; ++i) {
	Vector(lambda_, i) = (1 - rhot_) * Vector(lambda_, i) +
	    rhot_ * (Vector(eta_, i) + coef * Vector(sstats_, i));
    }

    exp_dirichlet_expectation_Matrix(expElogbeta_, lambda_, num_topics_, num_words_);
    updatect_ += 1.0;

    end = std::chrono::system_clock::now();
    std::chrono::duration<double> elapsed_seconds = end-start;
    std::time_t end_time = std::chrono::system_clock::to_time_t(end);
    std::cout << "Finished minibatch: " << start_pos / batch_size_
	      << ", elapsed time: " << elapsed_seconds.count() << "s\n";
}

// Calculate log-likelihood
double VariationalBayes::calculate_heldout_perplexity(std::vector<int>& doc_index,
						      int start_pos,
						      int end_pos) {
    double sum = 0.0;
    double tot = 0.0;
    std::vector<double> lambda_norm(num_topics_);
    for(int k = 0; k < num_topics_; ++k) {
	for(int j = 0; j < num_words_; ++j) {
	    lambda_norm[k] += Matrix(lambda_, k, j, num_words_);
	}
    }

#pragma omp parallel for reduction (+:sum,tot)
    for (int pos = start_pos; pos < end_pos; ++pos) {
	int d = doc_index[pos];
	double theta_norm = 0.0;
	for(int k = 0; k < num_topics_; ++k) {
	    theta_norm += Matrix(theta_, d, k, num_topics_);
	}

	Doc *doc = corpus_->docs[d];
	for (int i = 0; i < doc->word_index.size(); ++i) {
            int w = doc->word_index[i];
            double num = 0;
	    for (int k = 0; k < num_topics_; ++k) {
	     	num += (Matrix(theta_, d, k, num_topics_) / theta_norm) * 
		    (Matrix(lambda_, k, w, num_words_) / lambda_norm[k]);
	    }
      	    sum += doc->word_count[i] * std::log(num);
	    tot += doc->word_count[i];
       	}
    }

    return std::exp(-sum / tot);
}

// Dump Model
void VariationalBayes::dump() {
    clock_t begin = clock();

    dump_model(theta_, lambda_, num_documents_, num_topics_, num_words_);

    clock_t end = clock();
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
    printf("write finished, time elapsed: %.2fs\n", elapsed_secs);
}
