#pragma once

#include <boost/generator_iterator.hpp>
#include <boost/math/special_functions/digamma.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/random.hpp>

#include <algorithm>
#include <cmath>
#include <cstdlib>
#include <vector>

#define Vector(V, X) (V[(X)])
#define Matrix(M, X, Y, C) (M[(X)*(C)+(Y)])

// For a vector theta ~ Dir(alpha), computes E[log(theta)] given alpha.
void exp_dirichlet_expectation_Vector(double *result, double *x, int len);
void exp_dirichlet_expectation_Matrix(double *result, double *x, int row, int col);
