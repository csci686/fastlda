#include "io.h"

#include <cstdio>
#include <string>
#include <utility>
#include <ctime>
#include <algorithm>

Corpus *read_corpus(std::string filename) 
{
	// FOR docword.enron.txt
	// Using iostream, 36 secs
	// Using C FILE, 11 secs
	// FOR docword.nytimes.txt
	// Using iostream, ??? secs
	// Using C FILE, 202 secs

	FILE *fp;
	const char *cfilename = filename.c_str();
	fp = fopen(cfilename, "r");

	if (fp != NULL) {
		int D, W, N, ret;
		ret = fscanf(fp, "%d%d%d", &D, &W, &N);

		Corpus *corpus = new Corpus();
		corpus->num_word = W;

		corpus->docs = std::vector<Doc *>(D);
		for (int i = 0; i < D; ++i) {
			Doc *doc = new Doc();
			corpus->docs[i] = doc;
		}	
		
		for (int k = 0; k < N; ++k) {
			int d, w, n;
			ret = fscanf(fp, "%d%d%d", &d, &w, &n);
			Doc *doc = corpus->docs[d - 1];
			for (int i = 0; i < n; ++i) {
				doc->words.push_back(w - 1);
			}
		}

		fclose(fp);
		return corpus;
	} else {
		return NULL;
	}
}

void dump_model(
	std::vector<std::vector<double> > &theta, 
	std::vector<std::vector<double> > &phi) 
{
	FILE *fp;
	std::string filename;
	int D = theta.size();
	int W = phi.size();
	int T = theta[0].size();

	clock_t begin = clock();

	filename = "doctopic.txt";
	fp = fopen(filename.c_str(), "w");
	if (fp != NULL) {
		for (int d = 0; d < D; ++d) {
			for (int k = 0; k < T; ++k) {
				fprintf(fp, "%.6f", theta[d][k]);
				if (k < T - 1) {
					fprintf(fp, ", ");
				} else {
					fprintf(fp, "\n");
				}
			}
		}
		fclose(fp);
	}

	filename = "topics.txt";
	fp = fopen(filename.c_str(), "w");
	if (fp != NULL) {
		for (int k = 0; k < T; ++k) {
        		int firstW = 100;
			auto cmp = [&phi, &k](int x, int y) {
			    return phi[x][k] > phi[y][k];
			};
			std::vector<int> word_index;

			for (int w = 0; w < W; ++w) {
   			        word_index.push_back(w);
				std::push_heap(word_index.begin(), word_index.end(), cmp);

			        if (w >= firstW) {
    				        std::pop_heap(word_index.begin(), word_index.end(), cmp);
         				word_index.pop_back();
			        }
			}

			std::sort(word_index.begin(), word_index.end(), cmp);

			for (int i = 0; i < firstW; ++i) {
				fprintf(fp, "%d:%.6f", word_index[i] + 1, phi[word_index[i]][k]);
				if (i == firstW - 1) {
					fprintf(fp, "\n");
				} else {
					fprintf(fp, ", ");
				}
			}
		}
		fclose(fp); 
	}

	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	printf("write finished, time elapsed: %.2fs\n", elapsed_secs);
}
