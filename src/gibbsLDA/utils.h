#ifndef _UTILS_H_
#define _UTILS_H_

#include <vector>

struct Doc {
  	std::vector<int> words;
};

struct Corpus {
  	// Number of words in the dictionary
  	int num_word;

  	// Documents
  	std::vector<Doc *> docs;
};

// Calculate log-likelihood
double calculate_log_likelihood(
	Corpus *corpus, 
	std::vector<std::vector<double> > &theta, 
	std::vector<std::vector<double> > &phi);

// Calculate perplexity
double calculate_perplexity(
	Corpus *corpus, 
	std::vector<std::vector<double> > &theta, 
	std::vector<std::vector<double> > &phi);

#endif