#include "io.h"

#include <ctime>
#include <iostream>
#include <string>

int main() {

	std::string inputFile = "../data/docword.nytimes.txt";

	std::cout << "Start timer" << std::endl;

	// Note this clock doesn't work in parallel setting
	clock_t begin = clock();
	Corpus *corpus = read_corpus(inputFile);
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

	std::cout << "Finished in: " << elapsed_secs << " seconds" << std::endl;
	std::cout << "Vocabulary: " << corpus->num_word << std::endl;
	std::cout << "Document: " << corpus->docs.size() << std::endl;
}