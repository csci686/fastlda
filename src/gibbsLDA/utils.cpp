#include "utils.h"

#include <cmath>

// Calculate log-likelihood
double calculate_log_likelihood(
	Corpus *corpus, 
	std::vector<std::vector<double> > &theta, 
	std::vector<std::vector<double> > &phi)
{
	double sum = 0;
	int D = theta.size(), W = phi.size();
	for (int d = 0; d < D; ++d) {
		Doc *doc = corpus->docs[d];
		for (int i = 0; i < doc->words.size(); ++i) {
			int w = doc->words[i];
			double num = 0;
			for (int k = 0; k < theta[d].size(); ++k) {
				num += theta[d][k] * phi[w][k];
			}
			sum += std::log(num);
		}
	}
	return sum;
}

// Calculate perplexity
double calculate_perplexity(
	Corpus *corpus, 
	std::vector<std::vector<double> > &theta, 
	std::vector<std::vector<double> > &phi)
{
	double sum = 0;
	int N = 0;
	int D = theta.size(), W = phi.size();
	for (int d = 0; d < D; ++d) {
		Doc *doc = corpus->docs[d];
		for (int i = 0; i < doc->words.size(); ++i) {
			int w = doc->words[i];
			double num = 0;
			for (int k = 0; k < theta[d].size(); ++k) {
				num += theta[d][k] * phi[w][k];
			}
			sum += std::log(num);
		}
		N += doc->words.size();
	}
	return std::exp(-1 * sum / N);
}