#include "io.h"
#include "gibbslda.h"

#include <cstdio>
#include <ctime>

int main(int argc, char *argv[])
{
	srand (time(NULL));

	if (argc != 4) {
		printf("Usage: ./fastLDA docword.txt iterations NumOfTopics\n");
		return 0;
	}

	std::string filename = std::string(argv[1]);
	int num_iterations = atoi(argv[2]);
	int num_topics = atoi(argv[3]);

	printf("begin to read file\n");
	clock_t begin = clock();
	Corpus *corpus = read_corpus(filename);
	clock_t end = clock();
	double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;
	printf("read finished, time elapsed: %.2fs\n", elapsed_secs);
	
	GibbsLDA *lda = new GibbsLDA();
	lda->train_model(corpus, num_iterations, num_topics);
	
	return 0;
}