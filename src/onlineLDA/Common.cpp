#include "Common.h"

using boost::math::digamma;

void exp_dirichlet_expectation_Vector(double *result, double *x, int len) {
    double sum = 0.0;
    for (int i = 0; i < len; ++i) {
	sum += Vector(x, i);
    }
    for (int i = 0; i < len; ++i) {
        Vector(result, i) = std::exp(digamma(Vector(x, i)) - digamma(sum));
    }
}

void exp_dirichlet_expectation_Matrix(double *result, double *x, int row, int col) {
    for (int i = 0; i < row; ++i) {
	double sum = 0.0;
	for (int j = 0; j < col; ++j) {
	    sum += Matrix(x, i, j, col);
	}
        for (int j = 0; j < col; ++j) {
            Matrix(result, i, j, col) = std::exp(digamma(Matrix(x, i, j, col)) - digamma(sum));
	}
    }
}
